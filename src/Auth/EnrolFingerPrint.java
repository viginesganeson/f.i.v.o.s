package Auth;

import FingerPrint.Verification;
import com.digitalpersona.uareu.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.*;

public class EnrolFingerPrint extends Application {
    private static final long serialVersionUID = 1;

    private static final String ACT_SELECTION = "selection";
    private static final String ACT_CAPTURE = "capture";
    private static final String ACT_STREAMING = "streaming";
    private static final String ACT_VERIFICATION = "verification";
    private static final String ACT_IDENTIFICATION = "identification";
    private static final String ACT_ENROLLMENT = "enrollment";
    private static final String ACT_EXIT = "exit";

    private JDialog m_dlgParent;
    private JTextArea m_textReader;

    private static ReaderCollection m_collection;
    private static Reader m_reader;
    private static Fmd enrollmentFMD;

    @Override
    public void start(Stage primaryStage) throws Exception {

        final int vgap = 5;
        final int width = 380;



        Label lblReader = new Label(" ");


        FlowPane paneTop = new FlowPane();
        paneTop.setPadding(new javafx.geometry.Insets(10,10,10,10));
        paneTop.setAlignment(Pos.BASELINE_CENTER);
        paneTop.getChildren().add(lblReader);
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        Button btnEnrollment = new Button("Run enrollment");
//        btnEnrollment.setActionCommand(ACT_ENROLLMENT);
//        btnEnrollment.addActionListener(this);



        Button btnVerification = new Button("Run verification");
//        btnVerification.setActionCommand(ACT_VERIFICATION);
//        btnVerification.addActionListener(this);


        gridPane.add(btnEnrollment,0,8,1,1);
        gridPane.add(btnVerification,0,9,1,1);

        BorderPane root = new BorderPane();
        root.setCenter(gridPane);
        root.setTop(paneTop);


        Scene scene = new Scene(root,800,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        try {
            m_collection = UareUGlobal.GetReaderCollection();
            m_collection.GetReaders();
        } catch (UareUException e1) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error getting collection");
            return;
        }

        m_reader = m_collection.get(0);

        //enrollmentFMD = FingerPrint.Enrollment.Run(m_reader);
        Verification.Run(m_reader,enrollmentFMD);
    }

    public static void main (String [] args) {
        launch(args);
    }
}
