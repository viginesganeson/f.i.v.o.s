package Auth;

import Module.Voters;
import Module.createAlert;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import java.sql.*;
import java.util.Base64;

public class Login extends Application  {

    public Login(){}

    private Label lblusername,lblpassword,lbltitle,lblerror;
    private TextField tfusername;
    private PasswordField tfpassword;
    private Button btnlogin;
    private Hyperlink hlsingup;
    private ImageView imageView;
    private Image image;

    @Override
    public void start(Stage primaryStage) throws Exception {

        image = new Image("Image/logospr.png",150,150,false,false);
        imageView = new ImageView();
        imageView.setImage(image);
        hlsingup = new Hyperlink("Register as new voter");
        lblerror =new Label();
        lbltitle = new Label("Fingerprint Voting System For Election Commision");
        lblpassword = new Label("Password");
        lblusername = new Label("IC No. / Staff ID");
        tfpassword = new PasswordField();
        tfusername = new TextField();
        btnlogin = new Button("Login");
        btnlogin.setMaxWidth(Double.MAX_VALUE);

        btnlogin.setId("btnlogin");
        lbltitle.setId("lbltitle");
        lblerror.setId("lblerror");

        FlowPane paneTop = new FlowPane();
        paneTop.setPadding(new Insets(10,10,10,10));
        paneTop.setAlignment(Pos.BASELINE_CENTER);
        paneTop.getChildren().add(lbltitle);
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        gridPane.add(imageView,0,1,2,1);
        gridPane.add(lblusername,0,2);
        gridPane.add(lblpassword,0,3);
        gridPane.add(tfusername,1,2);
        gridPane.add(tfpassword,1,3);
        gridPane.add(btnlogin,0,4,2,1);
        gridPane.add(hlsingup,0,5,2,1);
        gridPane.add(lblerror,0,7,2,1);

        gridPane.setAlignment(Pos.CENTER);
        GridPane.setHalignment(hlsingup,HPos.CENTER);
        GridPane.setHalignment(lblerror,HPos.CENTER);
        GridPane.setHalignment(imageView,HPos.CENTER);

        BorderPane root = new BorderPane();
        root.setCenter(gridPane);
        root.setTop(paneTop);

        Scene scene = new Scene(root,800,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        hlsingup.setOnMouseClicked(event -> {
            Register register = new Register();
            try {
                register.start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnlogin.setOnMouseClicked(event ->
                {
                    String email = tfusername.getText();
                    byte[] bytesEncoded = Base64.getEncoder().encode(tfpassword.getText().getBytes());
                    System.out.println("encoded value is " + new String(bytesEncoded));

                    if (email.isEmpty() || bytesEncoded == null) {
                        lblerror.setText("Empty Fields");
                    } else {
                        lblerror.setText("");
                        try {
                            ResultSet resultSet = new Voters().loginVoter(email,bytesEncoded);
                            if (resultSet.next()){
                                Voters.usertype = resultSet.getString("usertype");
                                Voters.name = resultSet.getString("username");
                                Voters.ic = resultSet.getString("useric");
                                Voters.parliamentCon = resultSet.getString("userP");
                                Voters.stateCon = resultSet.getString("userS");
                                Voters.email = resultSet.getString("useremail");

                                if (Voters.usertype.equals("VOTER")){
                                    new PostLogin(email, primaryStage).start(primaryStage);
                                } else if (Voters.usertype.equals("STAFF")){
                                   new PostLogin(email, primaryStage).start(primaryStage);
                                }
                            } else {
                                new createAlert(Alert.AlertType.NONE,"Unable to Login. Ic and password mismatch");
                                tfpassword.clear();
                                tfpassword.requestFocus();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

    }

    public static void main(String[] args) {
        launch(args);
    }

}
