package Auth;

import Auth.Login;
import FingerPrint.Testo;
import FingerPrint.Verification;
import Module.Parliament;
import Module.Voters;
import Module.createAlert;
import com.digitalpersona.uareu.*;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.*;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Properties;

public class Register extends Application {

    private Button btnback,btnregister;
    private TextField tfname,tfic,tfpostcode,tfcity,tfnationality,tfphone,tfemail;
    private PasswordField pf1,pf2;
    private TextArea taadd1;
    private ComboBox cbstate,cbpelecarliament,cbelecstate;
    private Label lbltitle,lblname,lblic,lbladd1,lblpostcode,lblcity,lblstate,lblnationality,lblphone,lblemail,lblParl,lblelecstate;
    private Label lblpass,lblcpass,lblerror;
    private ResultSet resultSetParliament,resultSetState;

    public Register (){}

    @Override
    public void start(Stage primaryStage) throws Exception {

        lbltitle = new Label("VOTER REGISTRATION");     lblname = new Label("Full Name");
        lblic = new Label("Identification No.");        lbladd1 = new Label("Address Line ");
        lblpostcode = new Label("Post Code");           lblstate = new Label("State");
        lblcity = new Label("City");                    lblnationality = new Label("Nationality");
        lblphone = new Label("Phone No");               lblemail = new Label("Email");
        lblParl = new Label("Parliament");              lblelecstate = new Label("State to Vote");
        lblpass = new Label("Password");                lblcpass = new Label("Confirm Password");
        lblerror = new Label("");

        tfname = new TextField();               tfic= new TextField();
        tfpostcode= new TextField();            tfcity= new TextField();
        tfnationality= new TextField();         taadd1 = new TextArea();
        tfphone = new TextField();              tfemail = new TextField();
        pf1 = new PasswordField();              pf2 = new PasswordField();


        cbstate = new ComboBox();
        cbstate.getItems().addAll("Perlis","Kedah","Penang","Perak","Selangor");

        cbpelecarliament = new ComboBox();
        resultSetParliament = new Parliament().getAllParliament();
        while (resultSetParliament.next()){
            cbpelecarliament.getItems().add(resultSetParliament.getString("parliamentid")+" - "+resultSetParliament.getString("parliamentname"));

        }

        cbelecstate = new ComboBox();
        cbpelecarliament.setOnAction(event -> {
            try {
                cbelecstate.getItems().clear();

                resultSetState = new Parliament().getStateBasedOnParliament(cbpelecarliament.getValue().toString().substring(0,4));
                while (resultSetState.next()){
                    cbelecstate.getItems().add(resultSetState.getString("stateid")+" - "+resultSetState.getString("statename"));

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });

        btnback = new Button("Back");
        btnregister = new Button("Register");

        tfname.setPrefWidth(200);
        taadd1.setMaxWidth(Double.MAX_VALUE);
        tfic.setPrefWidth(200);
        cbstate.setPrefWidth(200);
        cbelecstate.setPrefWidth(200);
        cbpelecarliament.setPrefWidth(200);
        taadd1.setPrefRowCount(2);
        btnregister.setMaxWidth(Double.MAX_VALUE);
        btnback.setMaxWidth(Double.MAX_VALUE);

        lbltitle.setId("lbltitle");
        btnback.setId("btnback");
        btnregister.setId("btnregister");
        lblerror.setId("lblerror");

        FlowPane topPane = new FlowPane();
        topPane.setAlignment(Pos.CENTER);
        topPane.setPadding(new Insets(10,0,10,5));
        topPane.getChildren().addAll(lbltitle);

        GridPane centerPane = new GridPane();
        centerPane.setHgap(10);
        centerPane.setVgap(10);
        centerPane.setAlignment(Pos.TOP_CENTER);
        centerPane.setPadding(new Insets(10, 10, 10, 10));

        centerPane.add(lblname,0,0);        centerPane.add(tfname,1,0);
        centerPane.add(lblic,2,0);          centerPane.add(tfic,3,0);
        centerPane.add(lbladd1,0,1);        centerPane.add(taadd1,1,1,3,1);
        centerPane.add(lblcity,0,2);        centerPane.add(tfcity,1,2);
        centerPane.add(lblpostcode,2,2);    centerPane.add(tfpostcode,3,2);
        centerPane.add(lblstate,0,3);       centerPane.add(cbstate,1,3);
        centerPane.add(lblnationality,2,3); centerPane.add(tfnationality,3,3);
        centerPane.add(lblphone,0,4); centerPane.add(tfphone,1,4);
        centerPane.add(lblemail,2,4); centerPane.add(tfemail,3,4);
        centerPane.add(lblParl,0,5); centerPane.add(cbpelecarliament,1,5);
        centerPane.add(lblelecstate,2,5); centerPane.add(cbelecstate,3,5);
        centerPane.add(lblpass,0,6); centerPane.add(pf1,1,6);
        centerPane.add(lblcpass,2,6); centerPane.add(pf2,3,6);
        centerPane.add(lblerror,0,7,5,1);
        centerPane.add(btnregister,0,8,5,1);
        centerPane.add(btnback,0,9,5,1);
        GridPane.setHalignment(lblerror,HPos.CENTER);

        BorderPane root = new BorderPane();
        root.setTop(topPane);
        root.setCenter(centerPane);

        Scene scene = new Scene(root,700,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.setTitle("Register");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        btnback.setOnMouseClicked(event -> {
            Login login = new Login();
            try {
                login.start(primaryStage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }) ;

        btnregister.setOnMouseClicked(event -> {
            String name =tfname.getText();           String ic = tfic.getText();
            String add = taadd1.getText();           String city = tfcity.getText();
            String postcode = tfpostcode.getText();  String state = cbstate.getValue().toString();
            String phone = tfphone.getText();        String national = tfnationality.getText();
            String email = tfemail.getText();        String parli = cbpelecarliament.getValue().toString().substring(0,4);
            String stat = cbelecstate.getValue().toString().substring(0,3);    String ps = pf1.getText();
            String cps = pf2.getText();



            if(name.isEmpty() || ic.isEmpty() || add.isEmpty() || city.isEmpty() ||  postcode.isEmpty() ||
                    state.equals(null) || phone.isEmpty() || national.isEmpty() || email.isEmpty() ||
                    parli.equals(null) || stat.equals(null)  || ps.isEmpty() || cps.isEmpty() ) {
                lblerror.setText("Emp");
            }

            else if(!ps.equals(cps)){
                lblerror.setText("Password does not match");
            }

            else{
                lblerror.setText("");
                try {

                    String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    SecureRandom rnd = new SecureRandom();

                    StringBuilder sb = new StringBuilder(8);
                    for( int i = 0; i < 8; i++ ){
                        sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
                    }
                    byte[] bytesPassword = Base64.getEncoder().encode(ps.getBytes());
                    byte[] bytesConfirmPassword = Base64.getEncoder().encode(cps.getBytes());
                    int res = new Voters().registerNewVoter(ic,name,add,city,postcode,state,national,phone,email,bytesConfirmPassword,sb.toString(),parli,stat);
                    if (res == 1){

                        sendEmail(email,sb.toString());
                        //create alert
                        new createAlert(Alert.AlertType.NONE,"Registration successfull and Email sent");

                        //go to postregisterstaging
                        new PostRegister(ic, primaryStage).start(primaryStage);

                    }
                    else {
                        new createAlert(Alert.AlertType.NONE,"Unable to Register");
                    }
                } catch (SQLException e) {
                    new createAlert(Alert.AlertType.NONE,e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

    }


    public static void main(String[] args) {
        launch(args);
    }

    private void sendEmail(String email,String sstn) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("malaysiaelectioncommision@gmail.com","fivos@18");
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("malaysiaelectioncommision"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("Registration of Voter");
            message.setText("Dear," +
                    "\n\n No spam to  email, please! Your sstn is :" + sstn);

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
