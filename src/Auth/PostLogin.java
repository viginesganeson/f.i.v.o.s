package Auth;

import FingerPrint.PostLoginStaging;
import Module.Constants;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.swing.*;
import java.lang.reflect.InvocationTargetException;


public class PostLogin extends Application {

    public String ic;
    public Stage stage;

    PostLogin(String ic, Stage stage){
        this.ic = ic;
        this.stage = stage;
        Constants.ic = this.ic;
        Constants.stage = this.stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        SwingNode swingNode = new SwingNode();
        createSwing(swingNode, primaryStage);
        FlowPane paneTop = new FlowPane();
        paneTop.setPadding(new Insets(100,100,100,100));
        paneTop.setAlignment(Pos.BASELINE_CENTER);
        paneTop.getChildren().add(swingNode);

        Pane pane = new Pane();
        pane.getChildren().add(paneTop);

        StackPane p = new StackPane();
        p.getChildren().add(swingNode);
        p.setPrefSize(400,400);
        StackPane.setAlignment(swingNode,Pos.CENTER);
        p.setPadding(new Insets(10,10,10,80));

        Scene scene = new Scene(p,300,200);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Start Enroll Fingerprint");

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight() )/ 4);
    }

    public static void main (String [] args){
        launch(args);
    }

    private void createSwing(final SwingNode swingNode, Stage stage) throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeAndWait(() -> swingNode.setContent(new PostLoginStaging(stage)));
    }

}
