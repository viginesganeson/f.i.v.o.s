package Admin;

import Module.*;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;


public class EnrolCandidate extends Application {

    private ListView<String> listViewParliment,listViewState,listPCandidate,listSCandidate;
    private ObservableList<String> parliamentlist,statelist,pcandidatelist,scandidatelist;
    private ObservableList<Image> imagePCandidate,imageSCandidate;
    private FileInputStream fileInputStream;
    private File selectedFile;
    private ResultSet resultSetState,resultCandidate,resultSetNCandidate;


    public EnrolCandidate(){

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        listViewParliment = new ListView<>();
        listViewState = new ListView<>();
        listPCandidate = new ListView<>();
        listSCandidate = new ListView<>();
        parliamentlist = FXCollections.observableArrayList();
        statelist = FXCollections.observableArrayList();
        pcandidatelist= FXCollections.observableArrayList();
        scandidatelist = FXCollections.observableArrayList();
        imageSCandidate = FXCollections.observableArrayList();
        imagePCandidate = FXCollections.observableArrayList();


        loadListView();

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10,10,10,10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.add(new Label("List of Parliament Constituency"), 0, 0);
        gridPane.add(new Label("List of Parliament Candidate"), 1, 0);
        gridPane.add(new Label("List of State Constituency"), 2, 0);
        gridPane.add(new Label("List of State Candidate"), 3, 0);
        gridPane.add(listViewParliment, 0, 1);
        gridPane.add(listViewState, 2, 1);
        gridPane.add(listPCandidate,1,1);
        gridPane.add(listSCandidate,3,1);
        gridPane.setAlignment(Pos.CENTER);

        BorderPane root = new BorderPane();
        root.setLeft(new AdminSideBar(primaryStage));
        root.setCenter(gridPane);

        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");

        primaryStage.setTitle("Enroll Candidate");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight() )/ 4);

    }

    public static void main(String[] args) {
        launch(args);
    }

    private void loadListView() throws SQLException{
        pcandidatelist.clear();

        ResultSet resultSetParliament = new Parliament().getAllParliament();
        while (resultSetParliament.next()){
            parliamentlist.add(resultSetParliament.getString("parliamentid")+" - "+resultSetParliament.getString("parliamentname"));
        }

        listViewParliment = new ListView<>(parliamentlist);
        listViewParliment.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends String> ov, String old_val, String new_val) -> {

                    try {
                        if(new_val != null){
                            statelist.clear();
                            resultSetState = new Parliament().getStateBasedOnParliament(new_val.substring(0,4));
                            while (resultSetState.next()){
                                statelist.add(resultSetState.getString("stateid")+" - "+resultSetState.getString("statename"));

                            }

                            pcandidatelist.clear();
                            imagePCandidate.clear();
                            scandidatelist.clear();
                            resultCandidate = new Candidate().getAllCandidate(new_val.substring(0,4));
                            while (resultCandidate.next()){
                                pcandidatelist.add(resultCandidate.getString("candidatename"));
                                imagePCandidate.add(new Image(resultCandidate.getBinaryStream("candidateimage")));

                            }
                        }


                    } catch (SQLException e) {
                        e.printStackTrace();
                    }


                });

        listViewState = new ListView<>(statelist);
        listViewState.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    try {

                        if(newValue != null){
                            scandidatelist.clear();
                            imageSCandidate.clear();
                            resultSetNCandidate = new Candidate().getAllCandidate(newValue.substring(0,3));

                            while (resultSetNCandidate.next()){

                                scandidatelist.add(resultSetNCandidate.getString("candidatename"));
                                imageSCandidate.add(new Image(resultSetNCandidate.getBinaryStream("candidateimage")));

                            }

                        }


                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });



        listPCandidate = new ListView<>(pcandidatelist);
        listSCandidate = new ListView<>(scandidatelist);

        listPCandidate.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem viewCandidate = new MenuItem();
            viewCandidate.textProperty().bind(Bindings.format("View %s", cell.itemProperty()));
            viewCandidate.setOnAction(event ->{
               viewParlimentCandidate(cell);
            });

            MenuItem editCandidate = new MenuItem();
            editCandidate.textProperty().bind(Bindings.format("Edit %s", cell.itemProperty()));
            editCandidate.setOnAction(event ->{
                editParlimentCandidate(cell);

            });

            MenuItem deleteCandidate = new MenuItem();
            deleteCandidate.textProperty().bind(Bindings.format("Delete %s", cell.itemProperty()));
            deleteCandidate.setOnAction(event -> {
                deleteParlimentCandidate(cell);
            });


            contextMenu.getItems().addAll(viewCandidate,editCandidate,deleteCandidate);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;
        });

        listSCandidate.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem viewCandidate = new MenuItem();
            viewCandidate.textProperty().bind(Bindings.format("View %s", cell.itemProperty()));
            viewCandidate.setOnAction(event ->{
                viewStateCandidate(cell);
            });

            MenuItem editCandidate = new MenuItem();
            editCandidate.textProperty().bind(Bindings.format("Edit %s", cell.itemProperty()));
            editCandidate.setOnAction(event ->{
                editStateCandidate(cell);

            });

            MenuItem deleteCandidate = new MenuItem();
            deleteCandidate.textProperty().bind(Bindings.format("Delete %s", cell.itemProperty()));
            deleteCandidate.setOnAction(event -> {
                deleteStateCandidate(cell);
            });


            contextMenu.getItems().addAll(viewCandidate,editCandidate,deleteCandidate);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;
        });



        listViewParliment.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem addCandidate = new MenuItem();
            addCandidate.textProperty().bind(Bindings.format("Add New Candidate %s", cell.itemProperty()));
            addCandidate.setOnAction(event -> {

                addNewParlimentCandidate(cell);
            });

            contextMenu.getItems().addAll(addCandidate);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;

        });

        listViewState.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem addCandidate = new MenuItem();
            addCandidate.textProperty().bind(Bindings.format("Add New Candidate %s", cell.itemProperty()));
            addCandidate.setOnAction(event -> {
                addNewStateCandidate(cell);
            });

            contextMenu.getItems().addAll(addCandidate);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;

        });

    }

    private void addNewStateCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog textInputDialogASCandidate = new TextInputDialog();
        textInputDialogASCandidate.setTitle("New State Candidate");
        textInputDialogASCandidate.setHeaderText("Add New Candidate for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        TextField textFieldPCandidate = new TextField();
        textFieldPCandidate.setPrefWidth(200);
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("GIF", "*.gif"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        Button buttonSelectImage = new Button("Select Image");
        ComboBox cbPCandidateParty = new ComboBox();
        try {
            ResultSet resultSetPartyName = new Party().getAllParty();
            while (resultSetPartyName.next()){
                cbPCandidateParty.getItems().add(resultSetPartyName.getString("partyname"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        Label pcode = new Label(item.substring(0,4));
        Label actionStatus = new Label("");
        grid.add(new Label("Candidate Name : " ),0,0);
        grid.add(textFieldPCandidate,1,0);
        grid.add(new Label("Candidate Party: " ),0,1);
        grid.add(cbPCandidateParty,1,1);
        grid.add(new Label("Candidate Image: "),0,2);
        grid.add(buttonSelectImage,1,2);
        grid.add(actionStatus,0,3,3,1);


        buttonSelectImage.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                try {
                    fileInputStream = new FileInputStream(selectedFile);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                actionStatus.setText("File selected: " + selectedFile.getName());
            }
            else {
                actionStatus.setText("File selection cancelled.");
            }
        });

        textInputDialogASCandidate.getDialogPane().setContent(grid);
        Stage stage = (Stage) textInputDialogASCandidate.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        textInputDialogASCandidate.setGraphic(null);
        Optional<String> result = textInputDialogASCandidate.showAndWait();
        result.ifPresent(res -> {
            String cname,cparty,cfor;
            cname = textFieldPCandidate.getText();
            cparty = cbPCandidateParty.getValue().toString();
            cfor = pcode.getText();
            try {
                if (new Candidate().addNewNCandidate(cname,cparty,cfor,fileInputStream) == 1){
                    statelist.clear();
                    parliamentlist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"New State Candidate Added");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }

    private void addNewParlimentCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog textInputDialogAddCP = new TextInputDialog();
        textInputDialogAddCP.setTitle("New Parliament Candidate");
        textInputDialogAddCP.setHeaderText("Add New Candidate for "+item);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        TextField textFieldPCandidate = new TextField();
        textFieldPCandidate.setPrefWidth(200);
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("GIF", "*.gif"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        Button buttonSelectImage = new Button("Select Image");
        ComboBox cbPCandidateParty = new ComboBox();
        try {
            ResultSet resultSetPartyName = new Party().getAllParty();
            while (resultSetPartyName.next()){
                cbPCandidateParty.getItems().add(resultSetPartyName.getString("partyname"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        Label pcode = new Label(item.substring(0,4));
        Label actionStatus = new Label("");
        grid.add(new Label("Candidate Name : " ),0,0);
        grid.add(textFieldPCandidate,1,0);
        grid.add(new Label("Candidate Party: " ),0,1);
        grid.add(cbPCandidateParty,1,1);
        grid.add(new Label("Candidate Image: "),0,2);
        grid.add(buttonSelectImage,1,2);
        grid.add(actionStatus,0,3,3,1);
        buttonSelectImage.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                try {
                    fileInputStream = new FileInputStream(selectedFile);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                actionStatus.setText("File selected: " + selectedFile.getName());
            }
            else {
                actionStatus.setText("File selection cancelled.");
            }
        });

        textInputDialogAddCP.getDialogPane().setContent(grid);
        Stage stage = (Stage) textInputDialogAddCP.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        textInputDialogAddCP.setGraphic(null);
        Optional<String> result = textInputDialogAddCP.showAndWait();
        result.ifPresent(res -> {
            String cname,cparty,cfor;
            cname = textFieldPCandidate.getText();
            cparty = cbPCandidateParty.getValue().toString();
            cfor = pcode.getText();
            try {
                if (new Candidate().addNewPCandidate(cname,cparty,cfor,fileInputStream) == 1){
                    statelist.clear();
                    parliamentlist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"New Parliament Candidate Added");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        });
    }

    private void viewParlimentCandidate(ListCell<String> cell){
        String item = cell.getItem();
        Label canparty = new Label();
        Label canName = new Label();
        Label canfor = new Label();
        ImageView canImage = new ImageView();

        Alert alert = new Alert(Alert.AlertType.NONE);

        try {
            ResultSet resultSetCanbyName = new Candidate().getAllCandidatebyName(item);
            while (resultSetCanbyName.next()){
                canparty.setText(resultSetCanbyName.getString("candidateparty"));
                canName.setText(resultSetCanbyName.getString("candidatename"));
                canfor.setText(resultSetCanbyName.getString("candidatefor"));
                Image image = new Image(resultSetCanbyName.getBinaryStream("candidateimage"));
                canImage.setImage(image);
                canImage.setFitWidth(100); canImage.setFitHeight(100);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GridPane gridPaneViewCan = new GridPane();
        gridPaneViewCan.add(new Label("Candidate Name : "),0,0);
        gridPaneViewCan.add(canName,1,0);
        gridPaneViewCan.add(new Label("Candidate Party : "),0,1);
        gridPaneViewCan.add(canparty,1,1);
        gridPaneViewCan.add(new Label("Candidate Parliament : "),0,2);
        gridPaneViewCan.add(canfor,1,2);
        gridPaneViewCan.add(canImage,2,0,1,4);
        alert.setTitle("View Parliment Candidate Details");
        alert.setHeaderText(null);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getDialogPane().setContent(gridPaneViewCan);
        alert.showAndWait();


    }

    private void editParlimentCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog editDialogCandidatePar = new TextInputDialog();
        editDialogCandidatePar.setTitle("Edit Parliament Candidate");
        editDialogCandidatePar.setHeaderText("Edit Parliament Candidate Details for "+item);
        TextField textFieldPCandidateName = new TextField();
        GridPane gridPaneEditCan = new GridPane();
        int pcandidateid = 0;
        try {
            ResultSet resultSetEditPCandidate = new Candidate().getAllCandidatebyName(item);
            while (resultSetEditPCandidate.next()){
                textFieldPCandidateName.setText(resultSetEditPCandidate.getString("candidatename"));
                pcandidateid = resultSetEditPCandidate.getInt("candidateid");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        gridPaneEditCan.add(new Label("Candidate Name : "),0,0);
        gridPaneEditCan.add(textFieldPCandidateName,1,0);
        editDialogCandidatePar.getDialogPane().setContent(gridPaneEditCan);
        Stage stage = (Stage) editDialogCandidatePar.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        editDialogCandidatePar.setGraphic(null);
        Optional<String> result = editDialogCandidatePar.showAndWait();
        int finalPcandidateid = pcandidateid;
        result.ifPresent(res -> {

            String candidateName = textFieldPCandidateName.getText();

            try {
                if (new Candidate().updateCandidate(candidateName, finalPcandidateid) == 1){
                    pcandidatelist.clear();
                    parliamentlist.clear();
                    statelist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"Candidate Details Updated");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteParlimentCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog deletedialogCandidatePar = new TextInputDialog();
        deletedialogCandidatePar.setTitle("Delete Candidate");
        deletedialogCandidatePar.setHeaderText("Delete Candidate "+item);
        GridPane gridPaneEditCan = new GridPane();
        int pcandidateid = 0;
        try {
            ResultSet resultSetEditPCandidate = new Candidate().getAllCandidatebyName(item);
            while (resultSetEditPCandidate.next()){

                pcandidateid = resultSetEditPCandidate.getInt("candidateid");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        gridPaneEditCan.add(new Label("Confirm Delete Candidate ? "),0,0);


        deletedialogCandidatePar.getDialogPane().setContent(gridPaneEditCan);
        Stage stage = (Stage) deletedialogCandidatePar.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        deletedialogCandidatePar.setGraphic(null);
        Optional<String> result = deletedialogCandidatePar.showAndWait();
        int finalPcandidateid = pcandidateid;
        result.ifPresent(res -> {

            try {
                if (new Candidate().deleteCandidate(finalPcandidateid) == 1){
                    pcandidatelist.clear();
                    parliamentlist.clear();
                    statelist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"Candidate Details Deleted");

                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }


    private void viewStateCandidate(ListCell<String> cell){
        String item = cell.getItem();
        Label canparty = new Label();
        Label canName = new Label();
        Label canfor = new Label();
        ImageView canImage = new ImageView();

        Alert alert = new Alert(Alert.AlertType.NONE);

        try {
            ResultSet resultSetCanbyName = new Candidate().getAllCandidatebyName(item);
            while (resultSetCanbyName.next()){
                canparty.setText(resultSetCanbyName.getString("candidateparty"));
                canName.setText(resultSetCanbyName.getString("candidatename"));
                canfor.setText(resultSetCanbyName.getString("candidatefor"));
                Image image = new Image(resultSetCanbyName.getBinaryStream("candidateimage"));
                canImage.setImage(image);
                canImage.setFitWidth(100); canImage.setFitHeight(100);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GridPane gridPaneViewCan = new GridPane();
        gridPaneViewCan.add(new Label("Candidate Name : "),0,0);
        gridPaneViewCan.add(canName,1,0);
        gridPaneViewCan.add(new Label("Candidate Party : "),0,1);
        gridPaneViewCan.add(canparty,1,1);
        gridPaneViewCan.add(new Label("Candidate State : "),0,2);
        gridPaneViewCan.add(canfor,1,2);
        gridPaneViewCan.add(canImage,2,0,1,4);
        alert.setTitle("View State Candidate Details");
        alert.setHeaderText(null);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getDialogPane().setContent(gridPaneViewCan);
        alert.showAndWait();
    }

    private void editStateCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog editDialogCandidatePar = new TextInputDialog();
        editDialogCandidatePar.setTitle("Edit State Candidate");
        editDialogCandidatePar.setHeaderText("Edit State Candidate Details for "+item);
        TextField textFieldPCandidateName = new TextField();
        GridPane gridPaneEditCan = new GridPane();
        int pcandidateid = 0;
        try {
            ResultSet resultSetEditPCandidate = new Candidate().getAllCandidatebyName(item);
            while (resultSetEditPCandidate.next()){
                textFieldPCandidateName.setText(resultSetEditPCandidate.getString("candidatename"));
                pcandidateid = resultSetEditPCandidate.getInt("candidateid");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        gridPaneEditCan.add(new Label("Candidate Name : "),0,0);
        gridPaneEditCan.add(textFieldPCandidateName,1,0);
        editDialogCandidatePar.getDialogPane().setContent(gridPaneEditCan);
        Stage stage = (Stage) editDialogCandidatePar.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        editDialogCandidatePar.setGraphic(null);
        Optional<String> result = editDialogCandidatePar.showAndWait();
        int finalPcandidateid = pcandidateid;
        result.ifPresent(res -> {

            String candidateName = textFieldPCandidateName.getText();

            try {
                if (new Candidate().updateCandidate(candidateName, finalPcandidateid) == 1){
                    pcandidatelist.clear();
                    parliamentlist.clear();
                    statelist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"Candidate Details Updated");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }

    private void deleteStateCandidate(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog deletedialogCandidatePar = new TextInputDialog();
        deletedialogCandidatePar.setTitle("Delete Candidate");
        deletedialogCandidatePar.setHeaderText("Delete Candidate "+item);
        GridPane gridPaneEditCan = new GridPane();
        int pcandidateid = 0;
        try {
            ResultSet resultSetEditPCandidate = new Candidate().getAllCandidatebyName(item);
            while (resultSetEditPCandidate.next()){

                pcandidateid = resultSetEditPCandidate.getInt("candidateid");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        gridPaneEditCan.add(new Label("Confirm Delete Candidate ? "),0,0);


        deletedialogCandidatePar.getDialogPane().setContent(gridPaneEditCan);
        Stage stage = (Stage) deletedialogCandidatePar.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        deletedialogCandidatePar.setGraphic(null);
        Optional<String> result = deletedialogCandidatePar.showAndWait();
        int finalPcandidateid = pcandidateid;
        result.ifPresent(res -> {

            try {
                if (new Candidate().deleteCandidate(finalPcandidateid) == 1){
                    pcandidatelist.clear();
                    parliamentlist.clear();
                    statelist.clear();
                    loadListView();
                    new createAlert(Alert.AlertType.NONE,"Candidate Details Deleted");

                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }
}
