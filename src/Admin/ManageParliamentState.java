package Admin;

import Module.*;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class ManageParliamentState extends Application {

    private Label lblparliament,lblstate;
    private Button btnAddParliament,btnAddState;
    private ListView<String> parliamentListView,stateListView;
    private ObservableList<String> stateList,parliamentList;
    private ListView<String> partylistView;
    private Button btnAddParty;
    private FileInputStream fileInputStream;
    private File selectedFile;
    private ObservableList<String> partyList;

    public ManageParliamentState() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        lblparliament = new Label("List of Parliament");
        lblstate = new Label("List of State");
        btnAddParliament = new Button("Add New Parliament");
        btnAddState = new Button("Add New State");
        partyList  = FXCollections.observableArrayList();
        partylistView = new ListView<>();
        btnAddParty = new Button("Add Party");

        btnAddParty.setPrefWidth(200);
        btnAddParliament.setPrefWidth(200);
        btnAddState.setPrefWidth(200);

        parliamentList = FXCollections.observableArrayList();
        stateList = FXCollections.observableArrayList();
        loadListView();

        VBox vBox = new VBox(); vBox.setSpacing(10);
        vBox.getChildren().addAll(btnAddParliament,btnAddState,btnAddParty);


        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.add(lblparliament, 0, 1);
        gridPane.add(lblstate, 1, 1);
        gridPane.add(new Label("List of Party"), 2, 1);
        gridPane.add(parliamentListView, 0, 2);
        gridPane.add(stateListView, 1, 2);
        gridPane.add(partylistView, 2, 2);
        gridPane.add(vBox,3,2);
        gridPane.setAlignment(Pos.CENTER);

        BorderPane root = new BorderPane();
        root.setCenter(gridPane);
        root.setLeft(new AdminSideBar(primaryStage));
        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.setTitle("Manage Parliament State");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();


        btnAddParliament.setOnMouseClicked(event -> {
            addParliment();
        });

        btnAddState.setOnMouseClicked(event -> {
           addState();
        });

        btnAddParty.setOnMouseClicked(event -> {
            addParty();
        });
    }

    public void main (String [] args){
        launch(args);
    }

    private void loadListView() throws SQLException {
        loadListViewParty();
        ResultSet resultSetParliament = new Parliament().getAllParliament();
        while (resultSetParliament.next()){
            parliamentList.add(resultSetParliament.getString("parliamentid")+" - "+resultSetParliament.getString("parliamentname"));

        }

        parliamentListView = new ListView<>(parliamentList);
        parliamentListView.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends String> ov, String old_val, String new_val) -> {
                    ResultSet resultSetState = null;
                    try {
                        stateList.clear();
                        if(new_val !=null){
                            resultSetState = new Parliament().getStateBasedOnParliament(new_val.substring(0,4));
                            while (resultSetState.next()){
                                stateList.add(resultSetState.getString("stateid")+" - "+resultSetState.getString("statename"));
                            }
                        }



                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });

        stateListView = new ListView<>(stateList);

        parliamentListView.setCellFactory(lv -> {

            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Edit %s", cell.itemProperty()));
            editItem.setOnAction(event -> {
                editParliment(cell);
            });


            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Delete %s", cell.itemProperty()));
            deleteItem.setOnAction(event -> {
                deleteParliment(cell);


            });

            contextMenu.getItems().addAll(editItem, deleteItem);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });

        stateListView.setCellFactory(lv -> {

            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem editItem = new MenuItem();
            editItem.textProperty().bind(Bindings.format("Edit %s", cell.itemProperty()));
            editItem.setOnAction(event -> {
                editState(cell);
            });


            MenuItem deleteItem = new MenuItem();
            deleteItem.textProperty().bind(Bindings.format("Delete %s", cell.itemProperty()));
            deleteItem.setOnAction(event -> {
                deleteState(cell);


            });

            contextMenu.getItems().addAll(editItem, deleteItem);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell ;
        });


    }

    private void loadListViewParty() throws SQLException {
        partyList.clear();
        ResultSet resultSetParty = new Party().getAllParty();

        while (resultSetParty.next()) {
            partyList.add(resultSetParty.getString("partyname"));
        }

        partylistView = new ListView<>(partyList);
        partylistView.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem viewParty = new MenuItem();
            viewParty.textProperty().bind(Bindings.format("View %s", cell.itemProperty()));
            viewParty.setOnAction(event ->{
                viewParty(cell);
            });

            MenuItem editParty = new MenuItem();
            editParty.textProperty().bind(Bindings.format("Edit %s", cell.itemProperty()));
            editParty.setOnAction(event ->{
                try {
                    editParty(cell);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            });

            MenuItem deleteParty = new MenuItem();
            deleteParty.textProperty().bind(Bindings.format("Delete %s", cell.itemProperty()));
            deleteParty.setOnAction(event -> {
                try {
                    deleteParty(cell);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });


            contextMenu.getItems().addAll(viewParty,editParty,deleteParty);
            cell.textProperty().bind(cell.itemProperty());
            cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
                if (isNowEmpty) {
                    cell.setContextMenu(null);
                } else {
                    cell.setContextMenu(contextMenu);
                }
            });
            return cell;
        });

    }

    private void addParliment(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add New Parliament");
        dialog.setHeaderText("Enter new Parliament Code and Parliament Name");
        dialog.setGraphic(null);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        TextField pcode = new TextField();
        pcode.setPromptText("Parliament Code");
        TextField pname = new TextField();
        pname.setPromptText("Parliament Name");

        grid.add(new Label("Parliament Code : "), 0, 0);
        grid.add(new Label("P "), 1, 0);
        grid.add(pcode, 2, 0);
        grid.add(new Label("Parliament Name : "), 0, 1);
        grid.add(pname, 1, 1,2,1);
        dialog.getDialogPane().setContent(grid);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        Optional<String> result = dialog.showAndWait();

        result.ifPresent(res -> {
            String pc = "P"+pcode.getText();
            String pn = pname.getText();
            try {
                if (new Parliament().addnewParliament(pc,pn) == 1)
                {
                    new createAlert(Alert.AlertType.NONE,"New Parliament Added");
                    stateList.clear();
                    parliamentList.clear();
                    loadListView();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void addState(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add New State");
        dialog.setHeaderText("Enter new State Code and State Name");

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        ComboBox pcode = new ComboBox();
        pcode.setPromptText("Parliament");
        try {
            pcode.getItems().clear();

            ResultSet resultState = new Parliament().getAllParliament();
            while (resultState.next()){
                pcode.getItems().add(resultState.getString("parliamentid")+" - "+resultState.getString("parliamentname"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        TextField scode = new TextField();
        scode.setPromptText("State Code");
        TextField sname = new TextField();
        sname.setPromptText("State Name");
        grid.add(new Label("Parliament  : "), 0, 0);
        grid.add(pcode, 1, 0,2,1);
        grid.add(new Label("State Code  : "), 0, 1);
        grid.add(new Label("N"),1,1);
        grid.add(scode, 2, 1);
        grid.add(new Label("State Name  : "), 0, 2);
        grid.add(sname, 1, 2,2,1);

        dialog.getDialogPane().setContent(grid);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        dialog.setGraphic(null);
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(res -> {

            String pc = pcode.getValue().toString().substring(0,4);
            int in = pcode.getSelectionModel().getSelectedIndex();
            String sn = sname.getText();
            String sc = "N"+scode.getText();
            try {
                 if (new Parliament().addnewState(sc,sn,pc) == 1 )
                 {
                     new createAlert(Alert.AlertType.NONE,"New State Added");
                     stateList.clear();
                     parliamentList.clear();
                     loadListView();
                     parliamentListView.getSelectionModel().select(in);
                     parliamentListView.getFocusModel().focus(in);
                     parliamentListView.scrollTo(in);

                 }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }

    private void editState(ListCell<String> cell){
        String item = cell.getItem();
        int itemlen = item.length();
        TextInputDialog editdialog = new TextInputDialog();
        editdialog.setTitle("Edit State");
        editdialog.setHeaderText("Edit State Details for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Label scode = new Label(item.substring(0,3));
        TextField sname = new TextField();
        sname.setText(item.substring(6,itemlen));
        grid.add(new Label("State Code"),0,0);
        grid.add(scode,1,0);
        grid.add(new Label("State Name"),0,1);
        grid.add(sname, 1, 1);
        Stage stage = (Stage) editdialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        editdialog.getDialogPane().setContent(grid);
        Optional<String> result = editdialog.showAndWait();
        result.ifPresent(res -> {

            String sc = scode.getText();
            String sn = sname.getText();

            try {
                if (new Parliament().updateState(sn,sc) == 1)
                {
                    new createAlert(Alert.AlertType.NONE,"State Details Updated");
                    stateList.clear();
                    parliamentList.clear();
                    loadListView();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }

    private void deleteState(ListCell<String> cell){
        String item = cell.getItem();
        TextInputDialog deleteDialog = new TextInputDialog();
        deleteDialog.setTitle("Delete State");
        deleteDialog.setHeaderText("Delete State Details for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Label pcode = new Label(item.substring(0,3));

        grid.add(new Label("Confirm delete " +item ),0,0);
        deleteDialog.setGraphic(null);
        Stage stage = (Stage) deleteDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        deleteDialog.getDialogPane().setContent(grid);
        Optional<String> result = deleteDialog.showAndWait();
        result.ifPresent(res -> {
            String pc = pcode.getText();
            try {
                if (new Parliament().deleteState(pc) == 1);{
                    new createAlert(Alert.AlertType.NONE,"State Details Deleted");
                    stateList.clear();
                    parliamentList.clear();
                    loadListView();

                }

            } catch (SQLException e) {
                new createAlert(Alert.AlertType.ERROR,e.getMessage());
            }
        });

    }

    private void editParliment(ListCell<String> cell){
        String item = cell.getItem();
        int itemlen = item.length();
        TextInputDialog editdialog = new TextInputDialog();
        editdialog.setTitle("Edit Parliament");
        editdialog.setHeaderText("Edit Parliament Details for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Label pcode = new Label(item.substring(0,4));
        TextField pname = new TextField();
        pname.setText(item.substring(7,itemlen));
        grid.add(new Label("Parliament Code"),0,0);
        grid.add(pcode,1,0);
        grid.add(new Label("Parliament Name"),0,1);
        grid.add(pname, 1, 1);
        editdialog.setTitle("Edit Parliament");
        Stage stage = (Stage) editdialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        editdialog.getDialogPane().setContent(grid);
        Optional<String> result = editdialog.showAndWait();
        result.ifPresent(res -> {

            String pc = pcode.getText();
            String pn = pname.getText();

            try {
                if (new Parliament().updateParliament(pn,pc) == 1)
                {
                    new createAlert(Alert.AlertType.NONE,"Parliment Details Updated");
                    stateList.clear();
                    parliamentList.clear();
                    loadListView();

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteParliment(ListCell<String> cell){

        String item = cell.getItem();
        TextInputDialog deleteDialog = new TextInputDialog();
        deleteDialog.setTitle("Delete Parliament");
        deleteDialog.setHeaderText("Delete Parliament Details for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Label pcode = new Label(item.substring(0,4));

        grid.add(new Label("Confirm delete " +item ),0,0);
        deleteDialog.setGraphic(null);
        Stage stage = (Stage) deleteDialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        deleteDialog.getDialogPane().setContent(grid);
        Optional<String> result = deleteDialog.showAndWait();
        result.ifPresent(res -> {
            String pc = pcode.getText();
            try {
                if (new Parliament().deleteParliament(pc) == 1);{
                    new createAlert(Alert.AlertType.NONE,"Parliament Details Deleted");
                    stateList.clear();
                    parliamentList.clear();
                    loadListView();

                }

            } catch (SQLException e) {
                new createAlert(Alert.AlertType.ERROR,e.getMessage());
            }
        });
    }

    private void addParty(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add New Party");
        dialog.setHeaderText("Enter new Party Name and Party Logo");
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        Label actionStatus = new Label("");
        TextField partyname = new TextField();
        partyname.setPromptText("Party Name");
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("GIF", "*.gif"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        Button button = new Button("Select File");
        button.setOnAction(e -> {
            selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                try {
                    fileInputStream = new FileInputStream(selectedFile);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                actionStatus.setText("File selected: " + selectedFile.getName());
            }
            else {
                actionStatus.setText("File selection cancelled.");
            }
        });
        grid.add(new Label("Party Name : "), 0, 0);
        grid.add(partyname, 1, 0);
        grid.add(new Label("Party Image : "), 0, 1);
        grid.add(button, 1, 1);
        grid.add(actionStatus, 0, 2,3,1);
        dialog.getDialogPane().setContent(grid);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        dialog.setGraphic(null);
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(res -> {
            String pn = partyname.getText();

            try {
                if (new Party().addNewParty(pn, fileInputStream) == 1){
                    new createAlert(Alert.AlertType.NONE,"New Party Added");
                    partyList.clear();
                    loadListView();

                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void viewParty(ListCell<String> cell){
        String item = cell.getItem();
        Label partyname = new Label();

        ImageView canImage = new ImageView();

        Alert alert = new Alert(Alert.AlertType.NONE);

        try {
            ResultSet resultSetGetParty = new Party().getAllPartyByName(item);
            while (resultSetGetParty.next()){
                partyname.setText(resultSetGetParty.getString("partyname"));
                Image image = new Image(resultSetGetParty.getBinaryStream("partyimage"));
                canImage.setImage(image);
                canImage.setFitWidth(200); canImage.setFitHeight(200);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GridPane gridPaneViewParty = new GridPane();
        gridPaneViewParty.add(new Label("Party Name : "),0,0);
        gridPaneViewParty.add(partyname,1,0);
        gridPaneViewParty.add(canImage,2,0);
        alert.setTitle("Party Details");
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        alert.setHeaderText(null);
        alert.setGraphic(null);
        alert.getButtonTypes().add(ButtonType.OK);
        alert.getDialogPane().setContent(gridPaneViewParty);
        alert.showAndWait().isPresent();

    }

    private void editParty(ListCell<String> cell) throws SQLException {
        String item = cell.getItem();

        TextInputDialog editParty = new TextInputDialog();
        editParty.setTitle("Edit Party");
        editParty.setHeaderText("Edit Party Details for "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        TextField pname = new TextField();
        ResultSet resultSetParty = new Party().getAllPartyByName(item);
        int partyid = 0;
        while (resultSetParty.next()){
            pname.setText(resultSetParty.getString("partyname"));
            partyid = resultSetParty.getInt("partyid");
        }
        grid.add(new Label("Party Name"),0,0);
        grid.add(pname,1,0);
        editParty.getDialogPane().setContent(grid);
        editParty.setGraphic(null);
        editParty.setTitle("Edit Party");
        Stage stage = (Stage) editParty.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        Optional<String> result = editParty.showAndWait();
        int finalPartyid = partyid;
        result.ifPresent(res -> {

            String pn = pname.getText();

            try {
                if (new Party().updateParty(finalPartyid,pn) == 1){
                    new createAlert(Alert.AlertType.NONE,"Party Details Updated");
                    partyList.clear();
                    loadListView();
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }

    private void deleteParty(ListCell<String> cell) throws SQLException{
        String item = cell.getItem();
        TextInputDialog editParty = new TextInputDialog();
        editParty.setTitle("Delete Party");
        editParty.setHeaderText("Delete Party "+item);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        ResultSet resultSetParty = new Party().getAllPartyByName(item);
        int partyid = 0;
        while (resultSetParty.next()){

            partyid = resultSetParty.getInt("partyid");
        }
        grid.add(new Label("Confirm Delete ?"),0,0);

        editParty.getDialogPane().setContent(grid);
        editParty.setGraphic(null);
        Stage stage = (Stage) editParty.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        Optional<String> result = editParty.showAndWait();
        int finalPartyid = partyid;
        result.ifPresent(res -> {


            try {
                if (new Party().deleteParty(finalPartyid) == 1){
                    new createAlert(Alert.AlertType.NONE,"Party Details Deleted");
                    partyList.clear();
                    loadListView();
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }
}
