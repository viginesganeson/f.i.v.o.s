package Admin;

import Module.AdminSideBar;
import Module.Parliament;
import Module.Voters;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import javafx.stage.Screen;
import javafx.stage.Stage;


public class Home extends Application {

    Label labelt;

    public Home(){
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox paneNumberOfParliment= new VBox();
        paneNumberOfParliment.setSpacing(10);
        Label lblNumberOfParliment = new Label();
        lblNumberOfParliment.setId("lblcount");
        lblNumberOfParliment.setText(Integer.toString(new Parliament().countParliment()));
        paneNumberOfParliment.getChildren().add(new Label("Total Number of Parliments Constituency"));
        paneNumberOfParliment.getChildren().add(lblNumberOfParliment);
        paneNumberOfParliment.setId("pane1");
        paneNumberOfParliment.setAlignment(Pos.CENTER);
        paneNumberOfParliment.setMinWidth(300);

        VBox paneNumberOfState= new VBox();
        paneNumberOfState.setSpacing(10);
        Label lblNumberOfState = new Label();
        lblNumberOfState.setId("lblcount");
        lblNumberOfState.setText(Integer.toString(new Parliament().countStare()));
        paneNumberOfState.getChildren().add(new Label("Total Number of State Constituency"));
        paneNumberOfState.getChildren().add(lblNumberOfState);
        paneNumberOfState.setId("pane1");
        paneNumberOfState.setAlignment(Pos.CENTER);
        paneNumberOfState.setMinWidth(300);

        VBox paneNumberOfVoter= new VBox();
        paneNumberOfState.setSpacing(10);
        Label lblNumberOfVoter = new Label();
        lblNumberOfVoter.setId("lblcount");
        lblNumberOfVoter.setText(Integer.toString(new Voters().countVoter()));
        paneNumberOfVoter.getChildren().add(new Label("Total Number of Voters"));
        paneNumberOfVoter.getChildren().add(lblNumberOfVoter);
        paneNumberOfVoter.setId("pane1");
        paneNumberOfVoter.setAlignment(Pos.CENTER);
        paneNumberOfVoter.setMinWidth(300);



        GridPane gridPane = new GridPane();
        gridPane.setHgap(50);
        gridPane.setVgap(50);
        gridPane.add(paneNumberOfParliment,0,0);
        gridPane.add(paneNumberOfState,1,0);
        gridPane.add(paneNumberOfVoter,2,0);
        gridPane.setAlignment(Pos.CENTER);



        BorderPane root = new BorderPane();
        root.setCenter(gridPane);
        root.setLeft(new AdminSideBar(primaryStage));


        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Staff Home");
        primaryStage.setScene(scene);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight() )/ 4);


    }
    public static void main(String[] args) {
        launch(args);
    }
}
