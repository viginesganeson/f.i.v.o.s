package Admin;

import Module.AdminSideBar;
import Module.Candidate;
import Module.Vote;
import Module.VoteList;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenerateReport extends Application {

    private TableView table = new TableView();
    @Override
    public void start(Stage primaryStage) throws Exception {


        ResultSet resultSet = new Candidate().getAllCan();
        TableColumn canCol = new TableColumn("CandidateName");
        TableColumn parCol = new TableColumn("Parliament/State");
        TableColumn statCol = new TableColumn("State");
        TableColumn sCol = new TableColumn("State Vote Count");
        TableColumn pCol = new TableColumn("Parliament Vote Count");
        ObservableList<VoteList> data = FXCollections.observableArrayList();

        while (resultSet.next()){
            String cname,scount = null,pcount =null;
            System.out.println("Candidateid : " +resultSet.getString("candidatename"));
            String sname = null;
            String pname = null;
            cname = resultSet.getString("candidatename");
            sname = resultSet.getString("candidatefor");
            pname = resultSet.getString("candidatefor");
            ResultSet resultSet1 = new Vote().getCandidateParliCount(resultSet.getInt("candidateid"));
            while (resultSet1.next()){
                System.out.println("Count : " +resultSet1.getInt("parlicount"));
                pcount = Integer.toString(resultSet1.getInt("parlicount"));
            }
            ResultSet resultSet2 = new Vote().getCandidateStateCount(resultSet.getInt("candidateid"));
            while (resultSet2.next()){
                System.out.println("Count : " + resultSet2.getInt("statecount"));
                scount = Integer.toString(resultSet2.getInt("statecount"));
            }

            data.add(new VoteList(cname,pname,sname,scount,pcount));
            canCol.setCellValueFactory(new PropertyValueFactory<VoteList, String>("cname"));
            parCol.setCellValueFactory(new PropertyValueFactory<VoteList, String>("pname"));
            sCol.setCellValueFactory(new PropertyValueFactory<VoteList, String>("scount"));
            pCol.setCellValueFactory(new PropertyValueFactory<VoteList, String>("pcount"));
        }

        table.setItems(data);
        table.getColumns().addAll(canCol, parCol, pCol, sCol);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll( table);
        BorderPane root = new BorderPane();
        root.setLeft(new AdminSideBar(primaryStage));
        root.setCenter(vbox);

        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");

        primaryStage.setTitle("Generate Report");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight() )/ 4);
    }

    public static void main(String [] args){
        launch(args);
    }
}
