package FingerPrint;

import Module.Constants;
import com.digitalpersona.uareu.*;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;

public class Testo extends Application {

    private static ReaderCollection m_collection;
    private static Reader m_reader;
    private static Fmd enrollmentFMD;
    public String ic;
    public Stage stage;

    public Testo (String ic, Stage stage){
        this.ic = ic;
        this.stage = stage;
        Constants.ic = this.ic;
        Constants.stage = this.stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {

        final SwingNode swingNode = new SwingNode();
        createSwing(swingNode);
        FlowPane paneTop = new FlowPane();
        paneTop.setPadding(new Insets(100,100,100,100));
        paneTop.setAlignment(Pos.BASELINE_CENTER);
        paneTop.getChildren().add(swingNode);;


        Pane pane = new Pane();
        pane.getChildren().add(paneTop);

        StackPane p = new StackPane();
        p.getChildren().add(swingNode);
        p.setPrefSize(400,400);
        StackPane.setAlignment(swingNode,Pos.CENTER);
        p.setPadding(new Insets(10,10,10,80));

        Scene scene = new Scene(p,300,200);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Start Enroll Fingerprint");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main (String [] args){
        launch(args);
    }

    private void createSwing(final SwingNode swingNode){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                swingNode.setContent(new UareUSampleJava());
            }
        });
    }

}
