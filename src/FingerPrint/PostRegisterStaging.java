package FingerPrint;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Reader;
import com.digitalpersona.uareu.ReaderCollection;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import javafx.application.Platform;
import javafx.stage.Stage;

public class PostRegisterStaging extends JPanel implements ActionListener {
    private static final long serialVersionUID = 1;

    private static final String ACT_SELECTION = "selection";
    private static final String ACT_CAPTURE = "capture";
    private static final String ACT_STREAMING = "streaming";
    private static final String ACT_VERIFICATION = "verification";
    private static final String ACT_IDENTIFICATION = "identification";
    private static final String ACT_ENROLLMENT = "enrollment";
    private static final String ACT_EXIT = "exit";

    private JDialog m_dlgParent;
    private JTextArea m_textReader;

    private static ReaderCollection m_collection;
    private static Reader m_reader;
    private static Fmd enrollmentFMD;
    Stage stage;

    public PostRegisterStaging(Stage stage) {
        final int vgap = 5;
        final int width = 380;

        this.stage = stage;
        BoxLayout layout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        setLayout(layout);

        JButton btnEnrollment = new JButton("Run enrollment");
        btnEnrollment.setActionCommand(ACT_ENROLLMENT);
        btnEnrollment.addActionListener(this);
        btnEnrollment.setPreferredSize(new Dimension(40, 40));


        btnEnrollment.setForeground(Color.BLACK);
        btnEnrollment.setBackground(Color.WHITE);
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        btnEnrollment.setFocusPainted(false);
        btnEnrollment.setBorder(compound);
        btnEnrollment.requestFocus();
        add(btnEnrollment);
        add(Box.createVerticalStrut(vgap));

        setBackground(new Color(0,0,0,0));
        setOpaque(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals(ACT_VERIFICATION)) {

            try {
                this.m_collection = UareUGlobal.GetReaderCollection();
                m_collection.GetReaders();
            } catch (UareUException e1) {
                // TODO Auto-generated catch block
                JOptionPane.showMessageDialog(null, "Error getting collection");
                return;
            }

            if (m_collection.size() == 0) {
                MessageBox.Warning("Reader is not selected");
                return;
            }

            m_reader = m_collection.get(0);

            if (null == m_reader) {
                MessageBox.Warning("Reader is not selected");
            } else {
                Verification.Run(m_reader, this.enrollmentFMD);
            }
        }

        else if (e.getActionCommand().equals(ACT_ENROLLMENT)) {

            try {
                this.m_collection = UareUGlobal.GetReaderCollection();
                m_collection.GetReaders();

            } catch (UareUException e1) {
                // TODO Auto-generated catch block
                JOptionPane.showMessageDialog(null, "Error getting collection");
                return;
            }

            if (m_collection.size() == 0) {
                MessageBox.Warning("Reader is not selected");
                return;
            }

            m_reader = m_collection.get(0);

            if (null == m_reader) {
                MessageBox.Warning("Reader is not selected");
            } else {

                this.enrollmentFMD = Enrollment.Run(m_reader);
            }
        } else if (e.getActionCommand().equals(ACT_EXIT)) {
            m_dlgParent.setVisible(false);
        }
    }

    private void doModal(JDialog dlgParent) {
        m_dlgParent = dlgParent;
        m_dlgParent.setContentPane(this);
        m_dlgParent.pack();
        m_dlgParent.setLocationRelativeTo(null);
        m_dlgParent.setVisible(true);
        m_dlgParent.dispose();
    }

    public static void main(String[] args) throws IOException {

        try {
            m_collection = UareUGlobal.GetReaderCollection();
            m_collection.GetReaders();
        } catch (UareUException e1) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error getting collection");
            return;
        }

        m_reader = m_collection.get(0);

        //enrollmentFMD = FingerPrint.Enrollment.Run(m_reader);
        Verification.Run(m_reader,enrollmentFMD);

    }

}