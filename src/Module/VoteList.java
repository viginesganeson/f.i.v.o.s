package Module;

public class VoteList {

    String cname;
    String scount,pcount;
    String pname,sname;

    public VoteList(String cname,String pname, String sname, String  scount, String pcount){
        this.cname = cname;
        this.scount = scount;
        this.pcount = pcount;
        this.pname = pname;
        this.sname = sname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getScount() {
        return scount;
    }

    public void setScount(String scount) {
        this.scount = scount;
    }

    public String getPcount() {
        return pcount;
    }

    public void setPcount(String pcount) {
        this.pcount = pcount;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
