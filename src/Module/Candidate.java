package Module;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Candidate {

    Connection connection;
    PreparedStatement psAddNewPCandidate,psAddNewNCandidate,psGetCandidate,psGetCandidatebyName,psUpdateCandidate,psDeleteCandidate;

    public int addNewPCandidate(String cname,String cpar,String cfor,InputStream cimage) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psAddNewPCandidate = connection.prepareStatement("INSERT INTO tblcandidate (candidatename,candidateparty,candidatefor,candidateimage,candidatetype) VALUES (?,?,?,?,?)");
        psAddNewPCandidate.setString(1,cname);
        psAddNewPCandidate.setString(2,cpar);
        psAddNewPCandidate.setString(3,cfor);
        psAddNewPCandidate.setBinaryStream(4,cimage);
        psAddNewPCandidate.setString(5,"P");
        return psAddNewPCandidate.executeUpdate();
    }

    public int addNewNCandidate(String cname,String cpar,String cfor,InputStream cimage) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psAddNewNCandidate = connection.prepareStatement("INSERT INTO tblcandidate (candidatename,candidateparty,candidatefor,candidateimage,candidatetype) VALUES (?,?,?,?,?)");
        psAddNewNCandidate.setString(1,cname);
        psAddNewNCandidate.setString(2,cpar);
        psAddNewNCandidate.setString(3,cfor);
        psAddNewNCandidate.setBinaryStream(4,cimage);
        psAddNewNCandidate.setString(5,"N");
        return psAddNewNCandidate.executeUpdate();
    }

    public int updateCandidate(String cname,int cid) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psUpdateCandidate = connection.prepareStatement("UPDATE tblcandidate SET candidatename = ? WHERE candidateid = ? ");
        psUpdateCandidate.setString(1,cname);
        psUpdateCandidate.setInt(2,cid);

        return psUpdateCandidate.executeUpdate();
    }

    public int deleteCandidate(int cid) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psDeleteCandidate = connection.prepareStatement("DELETE FROM tblcandidate WHERE candidateid = ?");
        psDeleteCandidate.setInt(1,cid);
        return psDeleteCandidate.executeUpdate();
    }

    public ResultSet getAllCandidate(String pcode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psGetCandidate = connection.prepareStatement("SELECT * FROM tblcandidate WHERE candidatefor = ?");
        psGetCandidate.setString(1,pcode);
        ResultSet resultSet = psGetCandidate.executeQuery();
        return resultSet;
    }

    public ResultSet getAllCandidatebyName(String cname) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psGetCandidatebyName = connection.prepareStatement("SELECT * FROM tblcandidate WHERE candidatename = ?");
        psGetCandidatebyName.setString(1,cname);
        ResultSet resultSet = psGetCandidatebyName.executeQuery();
        return resultSet;
    }

    public ResultSet getAllCandidateByConst(String cons) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psGetCandidatebyName = connection.prepareStatement("SELECT * FROM tblcandidate WHERE candidatefor = ?");
        psGetCandidatebyName.setString(1,cons);
        ResultSet resultSet = psGetCandidatebyName.executeQuery();
        return resultSet;
    }

    public ResultSet getAllCan() throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psGetCandidate = connection.prepareStatement("SELECT * FROM tblcandidate");
        ResultSet resultSet = psGetCandidate.executeQuery();
        return resultSet;
    }
}
