package Module;


import Auth.Login;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.time.LocalDateTime;
import java.util.Optional;

public class VoterSideBar extends VBox {

    public VoterSideBar(Stage stage){

        setId("adminsidebar");
        //sideMenu.setSpacing(20);
        //sideMenu.setPadding(new Insets(10,10,10,10));
        Button btnHome = new Button("Home");
        Button btnLogout = new Button("Logout");
        Button btnProfile = new Button("Profile");
        btnHome.setPrefSize(150,50);
        btnLogout.setPrefSize(150,50);
        btnProfile.setPrefSize(150,50);
        Label labelt = new Label();
        Label labeld = new Label();
        labeld.setId("labeltimedate");
        labelt.setId("labeltimedate");

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {

            LocalDateTime today = LocalDateTime.now();

            int minute = today.getMinute();
            int second = today.getSecond();
            int hour = today.getHour();
            int year = today.getYear();
            int month = today.getMonthValue();
            int date = today.getDayOfMonth();
            labelt.setText(hour + ":" + minute + ":" + second);
            labeld.setText(date + "/" + month + "/" + year);
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();


        btnHome.setId("btnMenu");btnLogout.setId("btnMenu");btnProfile.setId("btnMenu");
        getChildren().addAll(labelt,labeld,btnHome,btnProfile, btnLogout);
        setAlignment(Pos.TOP_CENTER);

        btnHome.setOnAction(event -> {
            try {
                new Voter.Home().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnProfile.setOnAction(event -> {
            try {
                new Voter.Profile().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        btnLogout.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Logout");
            alert.setHeaderText(null);
            alert.setContentText("Are sure to logout from the system ? ");
            Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
            stage1.getIcons().add(new Image("Image/logospr.png"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    new Login().start(stage);
                    Voters.name = "";
                    Voters.usertype="";
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

}
