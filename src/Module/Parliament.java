package Module;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.scene.layout.BorderPane;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Parliament {

    Connection connection;
    PreparedStatement psGetAll,psGetStateBOP,psAddNewParliament,psAddNewState,psUpdateParliament,psDeleteParliament,psDeleteState;
    PreparedStatement psCountParliment,psUpdateState;
    String parliamentid,parliamentname;

    public int countParliment() throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psCountParliment = connection.prepareStatement("SELECT COUNT(*) FROM tblparliament");
        ResultSet rs1 = psCountParliment.executeQuery();
        rs1.next();
        return  rs1.getInt(1);
    }

    public int countStare() throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psCountParliment = connection.prepareStatement("SELECT COUNT(*) FROM tblstate");
        ResultSet rs1 = psCountParliment.executeQuery();

        rs1.next();
        return  rs1.getInt(1);
    }

    public ResultSet getAllParliament () throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psGetAll = connection.prepareStatement("SELECT * FROM tblparliament");
        ResultSet resultSet = psGetAll.executeQuery();
        return resultSet;
    }

    public ResultSet getStateBasedOnParliament(String parliamentid) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        psGetStateBOP = connection.prepareStatement("SELECT * FROM tblparliament,tblstate WHERE tblstate.parliamentid = ? AND tblparliament.parliamentid = tblstate.parliamentid");
        psGetStateBOP.setString(1,parliamentid);
        ResultSet resultSet = psGetStateBOP.executeQuery();
        return resultSet;
    }

    public int addnewParliament(String pcode,String pname) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psAddNewParliament = connection.prepareStatement("INSERT INTO tblparliament (parliamentid,parliamentname) VALUES (?,?)");
        psAddNewParliament.setString(1,pcode);
        psAddNewParliament.setString(2,pname);
        return psAddNewParliament.executeUpdate();
    }

    public int addnewState(String scode,String cname,String pcode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psAddNewState = connection.prepareStatement("INSERT INTO tblstate (stateid,statename,parliamentid) VALUES (?,?,?)");
        psAddNewState.setString(1,scode);
        psAddNewState.setString(2,cname);
        psAddNewState.setString(3,pcode);
        return psAddNewState.executeUpdate();
    }


    public int updateParliament(String pname,String pcode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psUpdateParliament = connection.prepareStatement("UPDATE tblparliament SET parliamentname = ? WHERE parliamentid = ?");
        psUpdateParliament.setString(1,pname);
        psUpdateParliament.setString(2,pcode);
        return psUpdateParliament.executeUpdate();
    }

    public int updateState(String sname,String scode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psUpdateState = connection.prepareStatement("UPDATE tblstate SET statename = ? WHERE stateid = ?");
        psUpdateState.setString(1,sname);
        psUpdateState.setString(2,scode);
        return psUpdateState.executeUpdate();
    }

    public int deleteParliament(String pcode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psDeleteParliament = connection.prepareStatement("DELETE FROM tblparliament WHERE parliamentid = ?");
        psDeleteParliament.setString(1,pcode);
        return psDeleteParliament.executeUpdate();
    }

    public int deleteState(String scode) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psDeleteState = connection.prepareStatement("DELETE FROM tblstate WHERE stateid = ?");
        psDeleteState.setString(1,scode);
        return psDeleteState.executeUpdate();
    }

}
