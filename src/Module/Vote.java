package Module;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;

public class Vote {

    private Connection connection;
    private PreparedStatement preparedStatement;

    public int addNewVoter(String voterIC,String voterState,String voterParli,int voterYear) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("INSERT INTO tblvote (voteric,voterstate,voterparli,voteryear) VALUES (?,?,?,?)");
        int v1 = Integer.parseInt(voterState);
        int v2 = Integer.parseInt(voterParli);

        preparedStatement.setString(1,voterIC);
        preparedStatement.setInt(2,v1);
        preparedStatement.setInt(3,v2);
        preparedStatement.setInt(4,voterYear);
        return preparedStatement.executeUpdate();
    }

    public ResultSet getVoterStatus(String ic) throws SQLException{
        int year = Year.now().getValue();
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS totcount FROM tblvote WHERE voteric = ? AND voteryear = ? ");
        preparedStatement.setString(1,ic);
        preparedStatement.setInt(2,year);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    public ResultSet getCandidateStateCount(int id) throws SQLException{
        int year = Year.now().getValue();
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT COUNT(*) as statecount FROM tblvote WHERE voterstate = ? AND voteryear = ? ");
        preparedStatement.setInt(1,id);
        preparedStatement.setInt(2,year);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    public ResultSet getCandidateParliCount(int id) throws SQLException{
        int year = Year.now().getValue();
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT COUNT(*) as parlicount FROM tblvote WHERE voterparli = ? AND voteryear = ? ");
        preparedStatement.setInt(1,id);
        preparedStatement.setInt(2,year);
        ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet;
    }
}
