package Module;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    public static String url = "jdbc:mysql://localhost:3306/fivos";
    public static String timezone = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    public static String username = "root";
    public static String password = "";
    public Connection connection ;

    public Connection startConnection()
    {
        try {
            connection = DriverManager.getConnection(DatabaseConnection.url+DatabaseConnection.timezone,DatabaseConnection.username,DatabaseConnection.password);

        }
        catch (CommunicationsException e) {
            System.out.print(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
