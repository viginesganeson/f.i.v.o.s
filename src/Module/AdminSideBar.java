package Module;

import Admin.EnrolCandidate;
import Admin.GenerateReport;
import Admin.Home;
import Admin.ManageParliamentState;
import Auth.Login;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.time.LocalDateTime;
import java.util.Optional;

public class AdminSideBar extends VBox {

    public AdminSideBar(Stage stage){

        setId("adminsidebar");
        //sideMenu.setSpacing(20);
        //sideMenu.setPadding(new Insets(10,10,10,10));
        Button btnHome = new Button("Home");
        Button btnEnroll = new Button("Enroll Candidate");
        Button btnManage = new Button("Manage");
        Button btnLogout = new Button("Logout");
        Button btnReport = new Button("Report");
        btnHome.setPrefSize(150,50);
        btnEnroll.setPrefSize(150,50);
        btnManage.setPrefSize(150,50);
        btnLogout.setPrefSize(150,50);
        btnReport.setPrefSize(150,50);



        Label labelt = new Label();
        Label labeld = new Label();
        labeld.setId("labeltimedate");
        labelt.setId("labeltimedate");

            Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {

                LocalDateTime today = LocalDateTime.now();

                int minute = today.getMinute();
                int second = today.getSecond();
                int hour = today.getHour();
                int year = today.getYear();
                int month = today.getMonthValue();
                int date = today.getDayOfMonth();
                labelt.setText(hour + ":" + minute + ":" + second);
                labeld.setText(date + "/" + month + "/" + year);
            }),
                    new KeyFrame(Duration.seconds(1))
            );
            clock.setCycleCount(Animation.INDEFINITE);
            clock.play();


        btnEnroll.setId("btnMenu"); btnManage.setId("btnMenu"); btnHome.setId("btnMenu");btnLogout.setId("btnMenu");btnReport.setId("btnMenu");
        getChildren().addAll(labelt,labeld,btnHome,btnEnroll,btnManage,btnReport,btnLogout);
        setAlignment(Pos.TOP_CENTER);

        btnHome.setOnAction(event -> {
            try {
                new Home().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnReport.setOnAction(event -> {
            try {
                new GenerateReport().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnEnroll.setOnAction(event -> {
            try {
                new EnrolCandidate().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        btnManage.setOnAction(event -> {
            try {
                new ManageParliamentState().start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnLogout.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Logout");
            alert.setHeaderText(null);
            alert.setContentText("Are sure to logout from the system ? ");
            Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
            stage1.getIcons().add(new Image("Image/logospr.png"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    new Login().start(stage);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

}
