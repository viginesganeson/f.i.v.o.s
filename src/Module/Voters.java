package Module;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Voters {

    Connection connection;
    PreparedStatement preparedStatement;
    public static String name;
    public static String usertype;
    public static String ic;
    public static String parliamentCon;
    public static String stateCon;
    public static String email;

    public int registerNewVoter(String ic, String name, String add, String city, String postcode, String state, String national, String phone, String email, byte[] passw, String sstn, String parli, String stat) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("INSERT INTO tbluser (useric,username,useradd,usercity,userpost,userstate,usernational,userphone,useremail,userpass,usertype,sstn,userP,userS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        preparedStatement.setString(1, ic);
        preparedStatement.setString(2, name);
        preparedStatement.setString(3, add);
        preparedStatement.setString(4, city);
        preparedStatement.setString(5, postcode);
        preparedStatement.setString(6, state);
        preparedStatement.setString(7, national);
        preparedStatement.setString(8, phone);
        preparedStatement.setString(9, email);
        preparedStatement.setBytes(10, passw);
        preparedStatement.setString(11, "VOTER");
        preparedStatement.setString(12, sstn);
        preparedStatement.setString(13, parli);
        preparedStatement.setString(14, stat);
        return preparedStatement.executeUpdate();

    }

    public int updateVoterProfile(String name, String ic,  String add, String city, String postcode, String national, String phone, String email, String oldIc) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("UPDATE tbluser SET useric = ?,username = ?,useradd = ? ,usercity = ?,userpost = ? ,usernational = ?,userphone = ?,useremail = ? WHERE useric = ?");
        preparedStatement.setString(1, ic);
        preparedStatement.setString(2, name);
        preparedStatement.setString(3, add);
        preparedStatement.setString(4, city);
        preparedStatement.setString(5, postcode);
        preparedStatement.setString(6, national);
        preparedStatement.setString(7, phone);
        preparedStatement.setString(8, email);
        preparedStatement.setString(9, oldIc);
        System.out.println(preparedStatement);
        return preparedStatement.executeUpdate();

    }

    public int countVoter() throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM tbluser WHERE usertype = 'VOTER'");
        ResultSet rs1 = preparedStatement.executeQuery();
        rs1.next();
        return rs1.getInt(1);
    }

    public ResultSet loginVoter(String useric, byte[] pass) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT * FROM tbluser WHERE useric = ? AND userpass = ? ");
        preparedStatement.setString(1, useric);
        preparedStatement.setBytes(2, pass);
        ResultSet rs1 = preparedStatement.executeQuery();
        System.out.println(preparedStatement);
        return rs1;
    }

    public ResultSet checkSSTN(String useric) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT sstn FROM tbluser WHERE useric = ?");
        preparedStatement.setString(1, useric);
        ResultSet rs1 = preparedStatement.executeQuery();
        return rs1;
    }

    public ResultSet getVoter(String useric) throws SQLException {
        connection = new DatabaseConnection().startConnection();
        preparedStatement = connection.prepareStatement("SELECT * FROM tbluser WHERE useric = ?");
        preparedStatement.setString(1, useric);
        ResultSet rs1 = preparedStatement.executeQuery();
        return rs1;
    }
}

