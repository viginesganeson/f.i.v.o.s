package Module;

import java.io.InputStream;
import java.sql.*;

public class Party {

    Connection connection;
    PreparedStatement psAddNewParty,psGetAllParty,psGetPartyImage,psUpdateParty,psDeleteParty;

    public int addNewParty(String pname,InputStream pimage) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psAddNewParty = connection.prepareStatement("INSERT INTO tblparty (partyname,partyimage) VALUES (?,?)");
        psAddNewParty.setString(1,pname);
        psAddNewParty.setBinaryStream(2,pimage);
        return psAddNewParty.executeUpdate();
    }

    public ResultSet getAllParty() throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psGetAllParty = connection.prepareStatement("SELECT * FROM tblparty");
        ResultSet resultSet = psGetAllParty.executeQuery();
        return resultSet;
    }

    public ResultSet getAllPartyByName(String pname) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psGetPartyImage = connection.prepareStatement("SELECT * FROM tblparty WHERE partyname = ?");
        psGetPartyImage.setString(1,pname);
        ResultSet resultSet = psGetPartyImage.executeQuery();
        return resultSet;
    }

    public int updateParty(int partyid,String partyname) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psUpdateParty = connection.prepareStatement("UPDATE tblparty SET partyname = ? WHERE partyid = ?");
        psUpdateParty.setString(1,partyname);
        psUpdateParty.setInt(2,partyid);
        return psUpdateParty.executeUpdate();
    }

    public int deleteParty(int partyid) throws SQLException{
        connection = new DatabaseConnection().startConnection();
        psDeleteParty = connection.prepareStatement("DELETE FROM tblparty WHERE partyid = ?");
        psDeleteParty.setInt(1,partyid);
        return psDeleteParty.executeUpdate();
    }
}
