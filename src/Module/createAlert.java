package Module;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class createAlert extends Alert {

    public createAlert(AlertType alertType,String content) {
        super(alertType);
        setTitle("Fingerprint Voting System");
        setHeaderText(null);
        setContentText(content);
        Stage stage = (Stage) getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logospr.png"));
        getButtonTypes().add(ButtonType.OK);
        showAndWait();

    }
}
