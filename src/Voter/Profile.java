package Voter;

import Auth.PostRegister;
import Module.*;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Profile extends Application {

    private Button btnback,btnregister;
    private TextField tfname,tfic,tfpostcode,tfcity,tfnationality,tfphone,tfemail;
    private PasswordField pf1,pf2;
    private TextArea taadd1;
    private ComboBox cbstate,cbpelecarliament,cbelecstate;
    private Label lbltitle,lblname,lblic,lbladd1,lblpostcode,lblcity,lblstate,lblnationality,lblphone,lblemail,lblParl,lblelecstate;
    private Label lblpass,lblcpass,lblerror;
    private ResultSet resultSetParliament,resultSetState;

    public Profile(){}

    @Override
    public void start(Stage primaryStage) throws Exception {

       // ResultSet resultSet = new Voters().getVoter("941208086873");
        ResultSet resultSet = new Voters().getVoter(Voters.ic);


        /*----------INITIALIZE-----------*/
        lblname = new Label("Full Name");
        lblic = new Label("Identification No.");        lbladd1 = new Label("Address Line ");
        lblpostcode = new Label("Post Code");           lblstate = new Label("State");
        lblcity = new Label("City");                    lblnationality = new Label("Nationality");
        lblphone = new Label("Phone No");               lblemail = new Label("Email");
        lblParl = new Label("Parliament");              lblelecstate = new Label("State to Vote");
        lblpass = new Label("Password");                lblcpass = new Label("Confirm Password");
        lblerror = new Label("");

        tfname = new TextField();               tfic= new TextField();
        tfpostcode= new TextField();            tfcity= new TextField();
        tfnationality= new TextField();         taadd1 = new TextArea();
        tfphone = new TextField();              tfemail = new TextField();


        cbstate = new ComboBox();
        cbstate.getItems().addAll("Perlis","Kedah","Penang","Perak","Selangor");

        cbpelecarliament = new ComboBox();
        resultSetParliament = new Parliament().getAllParliament();
        while (resultSetParliament.next()){
            cbpelecarliament.getItems().add(resultSetParliament.getString("parliamentid")+" - "+resultSetParliament.getString("parliamentname"));

        }

        cbelecstate = new ComboBox();

        cbpelecarliament.setOnAction(event -> {
            try {
                cbelecstate.getItems().clear();

                resultSetState = new Parliament().getStateBasedOnParliament(cbpelecarliament.getValue().toString().substring(0,4));
                while (resultSetState.next()){
                    cbelecstate.getItems().add(resultSetState.getString("stateid")+" - "+resultSetState.getString("statename"));

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        });

        while (resultSet.next()){
           tfname.setText(resultSet.getString("username"));
           tfic.setText(resultSet.getString("useric"));
           tfpostcode.setText(resultSet.getString("userpost"));
           tfcity.setText(resultSet.getString("usercity"));
           tfnationality.setText(resultSet.getString("usernational"));
           taadd1.setText(resultSet.getString("useradd"));
           tfphone.setText(resultSet.getString("useradd"));
           tfemail.setText(resultSet.getString("useremail"));
        }


        btnregister = new Button("Update");
        /*--------------------------*/


        /*----------SET SIZE-----------*/
        tfname.setPrefWidth(200);
        taadd1.setMaxWidth(Double.MAX_VALUE);
        tfic.setPrefWidth(200);
        cbstate.setPrefWidth(200);
        cbelecstate.setPrefWidth(200);
        cbpelecarliament.setPrefWidth(200);
        taadd1.setPrefRowCount(2);
        btnregister.setMaxWidth(Double.MAX_VALUE);

        btnregister.setId("btnregister");
        lblerror.setId("lblerror");


        FlowPane topPane = new FlowPane();
        topPane.setAlignment(Pos.CENTER);
        topPane.setPadding(new Insets(10,0,10,5));

        GridPane centerPane = new GridPane();
        centerPane.setHgap(10);
        centerPane.setVgap(10);
        centerPane.setAlignment(Pos.TOP_CENTER);
        centerPane.setPadding(new Insets(10, 10, 10, 10));

        centerPane.add(lblname,0,0);        centerPane.add(tfname,1,0);
        centerPane.add(lblic,2,0);          centerPane.add(tfic,3,0);
        centerPane.add(lbladd1,0,1);        centerPane.add(taadd1,1,1,3,1);
        centerPane.add(lblcity,0,2);        centerPane.add(tfcity,1,2);
        centerPane.add(lblpostcode,2,2);    centerPane.add(tfpostcode,3,2);
        centerPane.add(lblphone,0,4); centerPane.add(tfphone,1,4);
        centerPane.add(lblemail,2,4); centerPane.add(tfemail,3,4);
        centerPane.add(lblerror,0,7,5,1);
        centerPane.add(btnregister,0,8,5,1);

        GridPane.setHalignment(lblerror,HPos.CENTER);


        BorderPane root = new BorderPane();
        root.setCenter(centerPane);
        root.setLeft(new VoterSideBar(primaryStage));

        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.setTitle("Profile");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight() )/ 4);

        btnregister.setOnAction(event -> {
            String name =tfname.getText();           String ic = tfic.getText();
            String add = taadd1.getText();           String city = tfcity.getText();
            String postcode = tfpostcode.getText();  String phone = tfphone.getText();
            String national = tfnationality.getText();
            String email = tfemail.getText();

            int res = 0;
            try {
                res = new Voters().updateVoterProfile(name,ic,add,city,postcode,national,phone,email, Voters.ic);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (res == 1){
                new createAlert(Alert.AlertType.NONE,"Profile Updated");
                try {
                    new Profile().start(primaryStage);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            else {
                new createAlert(Alert.AlertType.NONE,"Unable to update profile");
            }
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
