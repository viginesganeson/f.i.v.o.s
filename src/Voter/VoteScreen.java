package Voter;


import Auth.Login;
import Auth.PostRegister;
import Module.*;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Optional;
import java.util.Properties;


public class VoteScreen extends Application {

    private ObservableList<String> pcandidatelist,scandidatelist;
    private ObservableList<Image> imagePCandidate,imageSCandidate;

    public String stateID;
    public String parlID;
    public VoteScreen(){

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        pcandidatelist = FXCollections.observableArrayList();
        scandidatelist = FXCollections.observableArrayList();
        imagePCandidate = FXCollections.observableArrayList();
        imageSCandidate = FXCollections.observableArrayList();

        ResultSet rsParliament = new Candidate().getAllCandidateByConst("P001");
        ResultSet rsState = new Candidate().getAllCandidateByConst("N01");
        int increP = 1;
        int increS = 1;
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        ToggleGroup radioGroup1 = new ToggleGroup();
        ToggleGroup radioGroup2 = new ToggleGroup();
        while (rsParliament.next()) {
            pcandidatelist.add(rsParliament.getString("candidatename"));
            imagePCandidate.add(new Image(rsParliament.getBinaryStream("candidateimage")));
            Image image = new Image(rsParliament.getBinaryStream("candidateimage"));

            ImageView imageView = new ImageView();
            imageView.setImage(image);
            imageView.setFitWidth(150); imageView.setFitHeight(150);
            gridPane.add(imageView,increP,1);
            RadioButton radioButton = new RadioButton(rsParliament.getString("candidatename"));
            radioButton.setId(Integer.toString(rsParliament.getInt("candidateid")));
            radioButton.setToggleGroup(radioGroup1);
            gridPane.add(radioButton,increP,2);
            increP++;
            radioButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (newValue){
                        System.out.println(radioButton.getText());
                        parlID = radioButton.getId();
                    }
                }
            });
        }

        while (rsState.next()){

            scandidatelist.add(rsState.getString("candidatename"));
            imageSCandidate.add(new Image(rsState.getBinaryStream("candidateimage")));
            Image image = new Image(rsState.getBinaryStream("candidateimage"));
            ImageView imageView = new ImageView();
            imageView.setImage(image);
            imageView.setFitWidth(150); imageView.setFitHeight(150);
            gridPane.add(imageView,increS,4);
            RadioButton radioButton = new RadioButton(rsState.getString("candidatename"));
            radioButton.setToggleGroup(radioGroup2);
            radioButton.setId(Integer.toString( rsState.getInt("candidateid")));
            gridPane.add(radioButton,increS,5);
            increS++;
            radioButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> obs, Boolean wasPreviouslySelected, Boolean isNowSelected) {
                    if (isNowSelected) {
                        System.out.println(radioButton.getText());
                        stateID = radioButton.getId();

                    } else {
                        // ...
                    }
                }
            });

        }

        gridPane.add(new Label("State Candidate"),2,3);
        gridPane.add(new Label("Parliament Candidate"),2,0);
        Button button = new Button("Vote");
        gridPane.add(button,2,6);
        gridPane.setAlignment(Pos.CENTER);

        BorderPane root = new BorderPane();
        root.setLeft(new VoterSideBar(primaryStage));
        root.setCenter(gridPane);

        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.setTitle("Voter Home");
        primaryStage.setScene(scene);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth())/2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight())/4);

        button.setOnMouseClicked(event -> {
            int year = Year.now().getValue();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm Vote");
            alert.setHeaderText(null);
            alert.setContentText("Are sure to cast selected vote");
            Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
            stage1.getIcons().add(new Image("Image/logospr.png"));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){

                int res = 0;
                try {
                    res = new Vote().addNewVoter(Voters.ic,stateID,parlID,year);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (res == 1){
                    new createAlert(Alert.AlertType.NONE,"Voting Successfull");
                    sendEmail(Voters.email);
                    try {
                        new Home().start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    new createAlert(Alert.AlertType.NONE,"Unable to Vote");
                }
            }
        });

    }


    public static void main(String[] args) {
        launch(args);
    }

    private void sendEmail(String email) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("malaysiaelectioncommision@gmail.com","fivos@18");
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("malaysiaelectioncommision"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("Vote Confirmation");
            message.setText("Dear," +
                    "\n\n No spam to  email, please! Your already cast your vote");

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}

