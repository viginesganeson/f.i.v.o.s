package Voter;


import Module.Vote;
import Module.VoterSideBar;
import Module.Voters;
import Module.createAlert;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;


public class Home extends Application {

    public Home(){

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FlowPane flowPane = new FlowPane();
        Label lblname = new Label();
        lblname.setText("Welcome "+ Voters.name);
        flowPane.getChildren().add(lblname);

        Label lblSSTN = new Label("Enter your SSTN");
        TextField textFieldSSTN = new TextField();
        Button btnVote = new Button("Proceed to vote");
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        ResultSet resultSet1 = new Vote().getVoterStatus(Voters.ic);
        if (resultSet1.next()){
            if (resultSet1.getInt("totcount") == 0){
                gridPane.add(lblSSTN,0,1,2,1);
                gridPane.add(textFieldSSTN,0,2);
                gridPane.add(btnVote,0,3);

            }else {
                gridPane.add(new Label("You already voted !!!"),0,1,2,1);
            }
        }

        gridPane.setAlignment(Pos.CENTER);
        GridPane.setHalignment(lblSSTN,HPos.CENTER);
        GridPane.setHalignment(textFieldSSTN,HPos.CENTER);
        GridPane.setHalignment(btnVote,HPos.CENTER);

        BorderPane root = new BorderPane();
        root.setLeft(new VoterSideBar(primaryStage));
        root.setCenter(gridPane);

        Scene scene = new Scene(root,1200,500);
        scene.getStylesheets().add("CSS/controls.css");
        primaryStage.getIcons().add(new Image("Image/logospr.png"));
        primaryStage.setTitle("Voter Home");
        primaryStage.setScene(scene);
        primaryStage.show();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth()- primaryStage.getWidth())/2);
        primaryStage.setY((screenBounds.getHeight()-primaryStage.getHeight())/4);

        btnVote.setOnMouseClicked(event -> {
            try {
                String sstnInput = textFieldSSTN.getText();
                if (sstnInput.isEmpty()){
                    new createAlert(Alert.AlertType.NONE,"Empty Fields.");
                }
                else {
                    ResultSet resultSet = new Voters().checkSSTN(Voters.ic);
                    String sstn = null;
                    if (resultSet.next()) {
                        sstn = resultSet.getString("sstn");
                        System.out.print(sstn);
                    }
                    if (sstnInput.equals(sstn)){
                        new createAlert(Alert.AlertType.NONE,"Match.");
                        new VoteScreen().start(primaryStage);
                    }
                    else {
                        new createAlert(Alert.AlertType.NONE,"Mismatch");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}

